import { Component, OnInit } from '@angular/core';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { Client } from '../../objects/client';
import { ClientService } from '../../services/client.service';
import { TicketService } from '../transaction-servir/ticketService';
import { CountryService } from './countiesService';
import { JobService } from './jobsService';


interface transfertType {
  name: string,
  code: number
}

interface identityType {
  name: string,
  code: number
}

interface titles{
  name: string,
  code: number
}


@Component({
  selector: 'app-transaction-emission',
  templateUrl: './transaction-emission.component.html',
  styleUrls: ['./transaction-emission.component.scss']
})
export class TransactionEmissionComponent implements OnInit {

  client:Client;
  titre:any;
  cliente:string;
  identity:string;
  property:any;
  steps:any = 1;

  transferts: transfertType[];
  identities:identityType[];

  selectedTransfertType: transfertType;
  selectedIdentityType:identityType;

  titles: titles[];
  cities: any[];
  professions:any[];
  frais:any[];
  beneficiaires:any[];

  personalInformation: any;
  

  constructor(private primengConfig: PrimeNGConfig, private countryService: CountryService, private jobService:JobService, private clientService:ClientService, public ticketService: TicketService,private messageService:MessageService) { 
    this.transferts = [
      {name: 'Espece', code: 1},
      {name: 'Débit', code: 2}
    ];

    this.identities = [
      {name: 'CIN', code: 1},
      {name: 'Passeport', code: 2},
      {name: 'Permis de Conduite', code: 3}
    ];

    this.titles = [
      {name:'Monsieur', code:1},
      {name: 'Madame', code:2}
    ];

    this.cities = [
      { name: "New York", code: "NY" },
      { name: "Rome", code: "RM" },
      { name: "London", code: "LDN" },
      { name: "Istanbul", code: "IST" },
      { name: "Paris", code: "PRS" }
    ];

    this.professions = [
      {name:"Medecin", code:"M"},
      {name:"Enseignant", code:"E"},
      {name:"Ingénieur", code:"I"},
      {name:"Autre", code:"A"},
    ];

    this.frais = [
      {name: "A la charge du client béneficiaire", code:0},
      {name: "A la charge du donneur d'ordre", code:1},
      {name: "Frais partagés", code:2}
    ];

    this.beneficiaires = [
      {name: "Elmeskaoui Amine", code:0},
      {name: "Afassy Brahim", code:1},
      {name: "Goumghar Oussama", code:2},
      {name: "Er-rajy Mohamed", code:3},
      {name: "Abdelhakim Amine", code:4},
      {name: "Chakiri Houssam", code:5}
    ]

  }



  ngOnInit(): void {
    this.primengConfig.ripple = true;
    // this.client = this.clientService.getProduct();
    this.personalInformation = this.ticketService.getTicketInformation().personalInformation;
    
  }

  searchClient():void
  {
    console.log(this.steps);
    // this.property = this.client.id;
    this.cliente = "client";
    // this.client = this.clientService.getProduct();
    // this.property = this.client.id;
    if (this.cliente == '' || this.selectedTransfertType == null || this.selectedIdentityType == null
    || this.identity == '') 
    {
      
      this.messageService.add({severity:'error', summary: 'Error', detail: 'Remplir tous les informations'});
    }

    else
    {
      console.log(this.cliente);
      console.log(this.selectedTransfertType);
      console.log(this.selectedIdentityType);
      console.log(this.identity);
    }

  }

  onSubmit()
  {
    if(this.steps < 3)
    {
      console.log(this.steps);
      this.steps = this.steps + 1; 
      console.log(this.titre);
    }
  }

  

  onNext()
  {
    if (this.cliente == '' || this.selectedTransfertType == null || this.selectedIdentityType == null
    || this.identity == '') 
    {
      this.messageService.add({severity:'success', summary:'Service Message', detail:'Via MessageService'});
    }

    else
    {
      this.steps = this.steps + 1;
    }
  }

  onBack()
  {
    this.steps = this.steps - 1;
    console.log(this.steps);
  }

  onAddBen()
  {
    this.steps = 4
    console.log(this.steps);
  }

  clear() {
    this.messageService.clear();
}

}
