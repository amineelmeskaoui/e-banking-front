import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Transaction } from '../objects/transaction';
import { Client } from '../admine/transaction-consultation/Client';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConsultationService {

  constructor(private http:HttpClient) { }

//   getConsultations() {
//     // return this.http.get<any>('https://api.afassy.tech/services/transactionapi/api/transactions')
//     //     .toPromise()
//     //     .then(res => <Transaction[]>res.data)
//     //     .then(data => { return data; });
//     // this.http.get<any>('http://api.afassy.tech/services/transactionapi/api/transactions').toPromise()
//     // .then(data => console.log(data));

// }

    getTranscations() {
      let host=environment.host
    return this.http.get<any>(host+"transactionapi/api/transactions")

    }
}
