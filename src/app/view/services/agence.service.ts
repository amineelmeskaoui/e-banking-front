import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Agence } from '../objects/agence';
import { Agent } from '../objects/agent';

@Injectable({
  providedIn: 'root'
})
export class AgenceService {

  host=environment.host

  constructor(private http:HttpClient) { }

  getAgences() {
      return this.http.get<any>(this.host+"accountapi/api/agences")
  }

  createAgence(agence:Agence){
      return this.http.post<any>(this.host + "accountapi/api/agences", agence);
  }
}
