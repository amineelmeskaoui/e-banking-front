import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Agent } from '../objects/agent';

@Injectable({
  providedIn: 'root'
})
export class AgentService {

  host=environment.host

  constructor(private http:HttpClient) { }

  getAgents() {
    return this.http.get<any>(this.host+"accountapi/api/agents")
   }

   createAgent(referenceAgent:any, agent:Agent)
   {
      return this.http.post<any>(this.host + "accountapi/api/agents/referece-agence/" + referenceAgent, agent);
   }
}
