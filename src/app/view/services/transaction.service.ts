import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Transaction } from '../objects/transaction';



@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  host=environment.host

  constructor(private http:HttpClient) {

  }

  getAllTransactions() {
    return this.http.get<any>(this.host+"transactionapi/api/transactions")
  }

  getTransactionsBloquees()
  {
    return this.http.get<any>(this.host+"transactionapi/api/transactions/blocked")
  }

  getTransactionsNonBloquees(){
    return this.http.get<any>(this.host+"transactionapi/api/transactions/not-blocked")
  }

  bloquerTansaction(transactions :any[]){
    return this.http.post<any>(this.host+"transactionapi/api/transactions/block", transactions)
  }

  debloquerTansaction(transactions:any[])
  {
    return this.http.post<any>(this.host+"transactionapi/api/transactions/unblock", transactions)
  }
}
