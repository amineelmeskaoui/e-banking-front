import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Product } from '../../agent/client-management/Product';
import { ProductService } from '../../agent/transaction-servir/product.service';
import { Agence } from '../../objects/agence';
import { AgenceService } from '../../services/agence.service';

@Component({
  selector: 'app-plafond-config',
  templateUrl: './plafond-config.component.html',
  styleUrls: ['./plafond-config.component.scss']
})
export class PlafondConfigComponent implements OnInit {

    agenceDialog: boolean;

    agences: Agence[];

    agence: Agence;

    selectedAgences: Agence[];

    submitted: boolean;

    statuses: any[];
    loading: boolean;
    errorMessage: string;
    newPlafondMontant:any;
    newPlafondTransaction:any;

  constructor(private agenceService: AgenceService, private messageService: MessageService, private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    // this.agenceService.getProducts().then(data => this.products = data);
    this.OnGetAgences();
  }

  OnGetAgences(){
    this.loading = true;
    this.errorMessage = "";
    this.agenceService.getAgences()
        .subscribe(
            (response) => {                           //next() callback
                console.log('response received')
                this.agences = response;
                console.log(this.agences)
            },
            (error) => {                              //error() callback
                console.error('Request failed with error')
                this.errorMessage = error;
                this.loading = false;
            },
            () => {                                   //complete() callback
                console.error('Request completed')      //This is actually not needed
                this.loading = false;
            })
}

  openNew() {
    this.agence = {};
    this.submitted = false;
    this.agenceDialog = true;
  }

  deleteSelectedProducts() {
    this.confirmationService.confirm({
        message: 'Are you sure you want to delete the selected products?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
            this.agences = this.agences.filter(val => !this.selectedAgences.includes(val));
            this.selectedAgences = null;
            this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
        }
    });
}

    editProduct(agence: Agence) {
        this.agence = {...this.agence};
        this.agenceDialog = true;
    }

    deleteProduct(agence: Agence) {
        this.confirmationService.confirm({
            message: 'Are you sure you want to delete ' + agence.reference + '?',
            header: 'Confirm',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.agences = this.agences.filter(val => val.id !== agence.id);
                this.agence = {};
                this.messageService.add({severity:'success', summary: 'Successful', detail: 'Agence Supprimée', life: 3000});
            }
        });
    }

    hideDialog() {
        this.agenceDialog = false;
        this.submitted = false;
    }

    saveAgence() {
        this.submitted = true;

        if (this.agence.reference.trim()) {
            if (this.agence.id) {
                this.agences[this.findIndexById(this.agence.id)] = this.agence;
                this.messageService.add({severity:'success', summary: 'Successful', detail: 'Agent modifié avec succès', life: 3000});
            }
            else {
                this.agence.id = this.createId();
                // this.agents.push(this.agent);
                this.agenceService.createAgence(this.agence).subscribe(data => {
                    alert('succsess')
                   console.log("data:"+data)
                  });
                console.log(this.agence);
                this.messageService.add({severity:'success', summary: 'Successful', detail: 'Agence ajoutée avec succès', life: 3000});
                window.location.reload();
            }
            this.agences = [...this.agences];
            this.agenceDialog = false;
            this.agence = {};
        }
    } 

    findIndexById(id: number): number {
        let index = -1;
        for (let i = 0; i < this.agences.length; i++) {
            if (this.agences[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    createId(): number {
        let id = '';
        var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for ( var i = 0; i < 5; i++ ) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return Number(id);
    }



}
