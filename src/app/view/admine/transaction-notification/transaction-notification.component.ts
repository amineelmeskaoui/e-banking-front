import { Component, OnInit } from '@angular/core';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { Transaction } from '../../objects/transaction';
import { TransactionService } from '../../services/transaction.service';

@Component({
  selector: 'app-transaction-notification',
  templateUrl: './transaction-notification.component.html',
  styleUrls: ['./transaction-notification.component.scss']
})
export class TransactionNotificationComponent implements OnInit {

  transactionGot:Transaction;
  transactions:Transaction;
  reference:any;
  enableNotif:any = false;
  alert:any;

  constructor(private transactionService:TransactionService, private messageService: MessageService, private primengConfig: PrimeNGConfig) { }

  ngOnInit(): void {
    // this.transactionService.getProducts().then(data => (this.transactionGot = data));
  }

  onSubmit()
  {
      
  }

  findByReference()
  {
    if(this.transactionGot != null)
    {
      this.transactions = this.transactionGot;
      this.enableNotif = true;
      console.log(this.transactionGot);
    }

    else
    {
      this.messageService.add({severity:'error', summary: 'Error', detail: 'Transaction non trouvé'});
    }
  }
  sendNotification()
  {

  }

  onReject() {
    this.messageService.clear('c');
  }

  clear() {
    this.messageService.clear();
  }

}
