import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TicketService } from '../../agent/transaction-servir/ticketService';
import { Client } from './Client';
import { ConsultationService } from '../../services/consultation.service';
import { Transaction } from '../../objects/transaction';
import { TransactionService } from '../../services/transaction.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
// import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-transaction-consultation',
  templateUrl: './transaction-consultation.component.html',
  styleUrls: ['./transaction-consultation.component.scss']
})
export class TransactionConsultationComponent implements OnInit {

  transactionDialog: boolean;

  transactions: any[];

  transaction: Transaction;

  selectedTransactions: Transaction[];

  submitted: boolean;

 

  statuses: any[];
  loading: any;
  errorMessage: any;

constructor(private transactionService: TransactionService, private messageService: MessageService, private confirmationService: ConfirmationService) { }

  ngOnInit() {
      // this.transactionService.getTransactions().then(data => this.transactions = data);
      this.OnGetTransction();
      console.log(this.transactions)
      this.statuses = [
          {label: 'INSTOCK', value: 'instock'},
          {label: 'LOWSTOCK', value: 'lowstock'},
          {label: 'OUTOFSTOCK', value: 'outofstock'}
      ];
  }

  OnGetTransction(){
      this.loading = true;
      this.errorMessage = "";
      this.transactionService.getAllTransactions()
          .subscribe(
              (response) => {                           //next() callback
                  console.log('response received')
                  this.transactions = response;
                  console.log(this.transactions)
              },
              (error) => {                              //error() callback
                  console.error('Request failed with error')
                  this.errorMessage = error;
                  this.loading = false;
              },
              () => {                                   //complete() callback
                  console.error('Request completed')      //This is actually not needed
                  this.loading = false;
              })
  }

  OnDebloquerTransaction()
  {
      
  }

  openNew() {
      this.transaction = {};
      this.submitted = false;
      this.transactionDialog = true;
  }

  deleteSelectedProducts() {
      this.confirmationService.confirm({
          message: 'Are you sure you want to delete the selected products?',
          header: 'Confirm',
          icon: 'pi pi-exclamation-triangle',
          accept: () => {
              this.transactions = this.transactions.filter(val => !this.selectedTransactions.includes(val));
              this.selectedTransactions = null;
              this.messageService.add({severity:'success', summary: 'Successful', detail: 'Transactions supprimées', life: 3000});
          }
      });
  }

  // editProduct(product: Product) {
  //     this.product = {...product};
  //     this.productDialog = true;
  // }

  deleteTransaction(product: Transaction) {
      this.confirmationService.confirm({
          message: 'Voulez vous supprimer le transaction  ' + this.transaction.reference + '?',
          header: 'Confirm',
          icon: 'pi pi-exclamation-triangle',
          accept: () => {
              this.transactions = this.transactions.filter(val => val.id !== this.transaction.id);
              this.transaction = {};
              this.messageService.add({severity:'success', summary: 'Successful', detail: 'Transaction supprimée', life: 3000});
          }
      });
  }

  public downloadPDF():void {
    var data = document.getElementById('pdfTable');
    html2canvas(data).then(canvas => {
        var imgWidth = 208;
        var imgHeight = canvas.height * imgWidth / canvas.width;
        const contentDataURL = canvas.toDataURL('image/png')
        let pdf = new jsPDF.default();
        var position = 0;
        pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
        pdf.save('TousTransactions.pdf');
    });

}

  hideDialog() {
      this.transactionDialog = false;
      this.submitted = false;
  }

  saveProduct() {
      this.submitted = true;

      if (this.transaction.reference.trim()) {
          if (this.transaction.id) {
              this.transactions[this.findIndexById(this.transaction.id)] = this.transaction;
              this.messageService.add({severity:'success', summary: 'Successful', detail: 'Information modifiées', life: 3000});
          }
          else {
              this.transaction.id = this.createId();
              // this.transaction.image = 'product-placeholder.svg';
              this.transactions.push(this.transaction);
              this.messageService.add({severity:'success', summary: 'Successful', detail: 'Transaction crée', life: 3000});
          }

          this.transactions = [...this.transactions];
          this.transactionDialog = false;
          this.transaction = {};
      }
  }

  findIndexById(id: string): number {
      let index = -1;
      for (let i = 0; i < this.transactions.length; i++) {
          if (this.transactions[i].id === id) {
              index = i;
              break;
          }
      }

      return index;
  }

  createId(): string {
      let id = '';
      var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      for ( var i = 0; i < 5; i++ ) {
          id += chars.charAt(Math.floor(Math.random() * chars.length));
      }
      return id;
  }



}
