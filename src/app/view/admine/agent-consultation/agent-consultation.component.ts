import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Agence } from '../../objects/agence';
import { Agent } from '../../objects/agent';
import { AgenceService } from '../../services/agence.service';
import { AgentService } from '../../services/agent.service';
import { MenuService } from '../../shared/slide-bar/app.menu.service';

@Component({
  selector: 'app-agent-consultation',
  templateUrl: './agent-consultation.component.html',
  styleUrls: ['./agent-consultation.component.scss']
})
export class AgentConsultationComponent implements OnInit {

    agents: Agent[];
    agences: Agence[]
    selectedAgents: Agent[];
    statuses: any[];

    agence:Agence;
    agent: Agent;

    submitted: boolean;
    loading: boolean;
    agentDialog: boolean;
    errorMessage: string;
    selectedReference: any;

  constructor(private agenceService: AgenceService, private agentService: AgentService,private messageService: MessageService, private confirmationService: ConfirmationService, private menuService:MenuService) { }

    ngOnInit(): void {
        this.OnGetAgents();
        this.OnGetAgences();
    
    }


    OnGetAgents(){
        this.loading = true;
        this.errorMessage = "";
        this.agentService.getAgents()
            .subscribe(
                (response) => {                           //next() callback
                    console.log('response received')
                    this.agents = response;
                    console.log(this.agents)
                },
                (error) => {                              //error() callback
                    console.error('Request failed with error')
                    this.errorMessage = error;
                    this.loading = false;
                },
                () => {                                   //complete() callback
                    console.error('Request completed')      //This is actually not needed
                    this.loading = false;
                })
        }
    
    OnGetAgences(){
        this.loading = true;
        this.errorMessage = "";
        this.agenceService.getAgences()
            .subscribe(
                (response) => {                           //next() callback
                    console.log('response received')
                    this.agences = response;
                    console.log(this.agences)
                },
                (error) => {                              //error() callback
                    console.error('Request failed with error')
                    this.errorMessage = error;
                    this.loading = false;
                },
                () => {                                   //complete() callback
                    console.error('Request completed')      //This is actually not needed
                    this.loading = false;
                })
    }

    onSelectType($event: any) {
        console.log($event)
        this.selectedReference = $event.reference;
    }


    openNew() {
        this.agent = {};
        this.submitted = false;
        this.agentDialog = true;
    }

    deleteSelectedProducts() {
        this.confirmationService.confirm({
            message: 'Are you sure you want to delete the selected products?',
            header: 'Confirm',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.agents = this.agents.filter(val => !this.selectedAgents.includes(val));
                this.selectedAgents = null;
                this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
            }
        });
    }

    editProduct(agent: Agent) {
        this.agent = {...this.agent};
        this.agentDialog = true;
    }

    deleteProduct(agent: Agent) {
        this.confirmationService.confirm({
            message: 'Are you sure you want to delete ' + agent.id + '?',
            header: 'Confirm',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.agents = this.agents.filter(val => val.id !== agent.id);
                this.agent = {};
                this.messageService.add({severity:'success', summary: 'Successful', detail: 'Agent Supprimée', life: 3000});
            }
        });
    }

    hideDialog() {
        this.agentDialog = false;
        this.submitted = false;
    }

    saveAgent() {
        this.submitted = true;

        if (this.agent.nom.trim()) {
            if (this.agent.id) {
                this.agents[this.findIndexById(this.agent.id)] = this.agent;
                this.messageService.add({severity:'success', summary: 'Successful', detail: 'Agent modifié avec succès', life: 3000});
            }
            else {
                this.agent.id = this.createId();
                // this.agents.push(this.agent);
                this.agentService.createAgent(this.selectedReference.reference, this.agent).subscribe(data => {
                    alert('succsess')
                   console.log("data:"+data)
                  });
                this.messageService.add({severity:'success', summary: 'Successful', detail: 'Agent ajouté avec succès', life: 3000});
                window.location.reload();
            }
            this.agents = [...this.agents];
            this.agentDialog = false;
            this.agent = {};
        }
    } 

    findIndexById(id: number): number {
        let index = -1;
        for (let i = 0; i < this.agents.length; i++) {
            if (this.agents[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    createId(): number {
        let id = '';
        var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for ( var i = 0; i < 5; i++ ) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return Number(id);
    }

}
