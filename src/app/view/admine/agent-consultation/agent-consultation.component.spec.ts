import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentConsultationComponent } from './agent-consultation.component';

describe('AgentConsultationComponent', () => {
  let component: AgentConsultationComponent;
  let fixture: ComponentFixture<AgentConsultationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgentConsultationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentConsultationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
