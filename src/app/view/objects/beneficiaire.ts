import { Client } from "./client";
import { KYC } from "./KYC";

export interface Benificiaire
{
    id:number;
    kyc?:KYC;
    client?:Client;
}