import { Agence } from "./agence";
import { Compte } from "./compte";

export class Agent
{
    id?:number;
    nom?:string;
    prenom?:string;
    login?:string;
    password?:string;
    compteAgent?:Compte;
    agence?:Agence;
}