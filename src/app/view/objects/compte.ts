import { Client } from "./client";

export interface Compte
{
    id?:number;
    solde?:number;
    dateCreation?:Date;
    status?:string;
    rib?:string;
    client?:Client;
}