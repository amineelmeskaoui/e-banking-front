export interface KYC
{
    id?:number;
    titre?:string;
    nom?:string;
    prenom?:string;
    typeIdentite?:string;
    numIdentite?:number;
    validateTimeIdentite?:Date;
    profession?:string;
    nationalite?:string;
    address?:string;
    gsm?:string;
    email?:string;
    

}