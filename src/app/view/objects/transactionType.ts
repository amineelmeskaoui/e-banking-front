export class TransactionType
{
    id?:number;
    type:String;
    plafondTransaction:number;
    plafondAnnuel:number;
}