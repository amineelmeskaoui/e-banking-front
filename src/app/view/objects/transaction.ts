import { Frais } from "./Frais";
import { Motif } from "./motif";
import { TransactionType } from "./transactionType";

export interface Transaction {

    id?:string;
    reference?:string;
    montant?:number;
    dateEmission?:Date;
    status?:string;
    pin?:number;
    notify?:boolean;
    loginAgent?:string;
    numClient?:string;
    nomBenificiair?:string;
    prenomBenificiair?:string;
    telephoneBenificiair?:string;
    transactionType?:TransactionType;
    motif?:Motif;
    frait?:Frais;
    
    // code?:string;
    // name?:string;
    // description?:string;
    // price?:number;
    // quantity?:number;
    // inventoryStatus?:string;
    // category?:string;
    // image?:string;
    // rating?:number;
}