export interface Agence
{
    id?:number;
    address?:string;
    ville?:string;
    reference?:string;
    plafondMontant?:number;
    plafondTransaction?:number;
    // id?:string;
    // code?:string;
    // name?:string;
    // description?:string;
    // price?:number;
    // quantity?:number;
    // inventoryStatus?:string;
    // category?:string;
    // image?:string;
    // rating?:number;
}